# Braço Robótico Arduíno

O projeto foi utilizado na apresentação final da disciplina de Mecanismos do curso de Mecatronica Industrial do IFCE, ministrada pela profa. Lorena Braga, consiste de uma aplicação em c para arduino, o projeto é composto por três servos motores com curso de 180º controlados por três potenciomentos.

Um dos servos possui o alcance limitado a 65º para evitar que atinja os outros, os outros possuem o alcance de 80º