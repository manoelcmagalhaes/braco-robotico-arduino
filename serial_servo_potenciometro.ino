#include <Servo.h>

// Servo 1

Servo servo1;

const int servo1PotPin = A0;

const int servo1Pin = 9;

int servo1_test;

// Servo 2

Servo servo2;

const int servo2PotPin = A2;

const int servo2Pin = 10;

int servo2_test;

// Servo 3

Servo servo3;

const int servo3PotPin = A4;

const int servo3Pin = 11;

int servo3_test;

// Inicio

void setup() {

servo1.attach(servo1Pin);

servo2.attach(servo2Pin);

servo3.attach(servo3Pin);

}

void loop() {

servo1_test = analogRead(servo1PotPin);

servo1_test = map(servo1_test, 0, 1023, 65, 0); //Rotação limitada a 65 graus

servo1.write(servo1_test);

servo2_test = analogRead(servo2PotPin);

servo2_test = map(servo2_test, 0, 1023, 80 , 0); //Rotação limitada a 80 graus

servo3.write(servo3_test);

servo3_test = analogRead(servo3PotPin);

servo3_test = map(servo3_test, 0, 1023, 80 , 0); //Rotação limitada a 80 graus

servo2.write(servo2_test);

delay(25);

}
